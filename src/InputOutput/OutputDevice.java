package InputOutput;

/**
 * Class used to print the gates result to standard output.
 * @author Maghiar Mihai Adrian
 *
 */
public class OutputDevice {
	public void printResult(String name, Boolean value){
		System.out.println(name + ": " + value);
	}
}
