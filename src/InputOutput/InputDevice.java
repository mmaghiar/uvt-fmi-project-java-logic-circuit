package InputOutput;

import java.util.Scanner;

public class InputDevice {
	private Scanner in;
	
	public InputDevice(){
		
	}
	/**
	 * 
	 * @param name - name to be printed at prompt
	 * @return a value read from standard input
	 * 
	 */
	public Boolean getValueFor(String name){
		in = new Scanner(System.in);
		System.out.print(name + " = ");
		String res = in.nextLine();
		if(res.equals("true"))
			return new Boolean(true);
		if(res.equals("false"))
			return new Boolean(false);
		in.close();
		
		return null;
	}
	
}
