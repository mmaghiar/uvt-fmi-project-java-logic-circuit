package input.csv;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import InputOutput.InputDevice;
import InputOutput.OutputDevice;

import circuit.ConstructCircuit;

public class File {
	/**
	 * A Buffer Reader is used in order to read the circuit file.
	 * This Buffer Reader further passed to be processed by the ConstructCircuit class
	 * 
	 * @param circuitFile - path to CSV file, containing the circuit description
	 * @param id - device used for input
	 * @param od - device used for output
	 */
	public void run(String circuitFile, InputDevice id, OutputDevice od){
		BufferedReader buffreader = null;
		try {
			buffreader = new BufferedReader(new FileReader(circuitFile));
			ConstructCircuit csvparse = new ConstructCircuit(id, od);
			csvparse.InputProcess(buffreader);
			buffreader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	//	return input;
	  }
}
