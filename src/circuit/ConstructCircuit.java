package circuit;

import gates.LogicalGate;
import gates.OnePinGate;
import gates.TwoPinGate;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import InputOutput.InputDevice;
import InputOutput.OutputDevice;

public class ConstructCircuit {
	private InputDevice id = null;
	private OutputDevice od = null;
	public ConstructCircuit(InputDevice id, OutputDevice od){
	this.id = id;
	this.od = od;
	}
	/**
	 * 
	 * Class used for processing the logical circuit on-the-fly, while reading the
	 * description in the csv file passed as argument to the constructor.
	 * A hash map is used in order to keep track of gates names and their result values.
	 * Every line in the csv which contains a gate description is composed by the gate
	 * name, the gate type, and depending on its type, one or two inputs.
	 * If the inputs are found in the hash map as a result of a gate, then that value
	 * is taken. Otherwise, the user is prompted to input a value for that specific
	 * pin on the gate.
	 * 
	 * The gates with no output used in another gate's input are treated as outputs
	 * of the entire circuit.
	 * 
	 * @param buffreader - used to pass the input
	 */
	public void InputProcess(BufferedReader buffreader){
		String line = "";
		String csvSplitBy = ",";
		Circuit circuit = new Circuit();
		Map<String, Boolean> gateValues = new HashMap<String, Boolean>();
		try {
			while ((line = buffreader.readLine()) != null) {
				if(line.contains("Gate"))
					continue;
				if(line.contains("Outputs")){
					String[] tokens = line.split(",");
					for(int i = 1; i < tokens.length; i++){
						od.printResult(tokens[i], gateValues.get(tokens[i]));
					}
				}else {
					String[] tokens = line.split(csvSplitBy);
					LogicalGate g = GateCreator.create(tokens[1], tokens[0]);
					circuit.addGate(g);
					if(g instanceof OnePinGate){
						String pin1 = tokens[2];
						if(gateValues.containsKey(pin1)){
							Boolean gResult = ((OnePinGate) g).process_input(gateValues.get(pin1));
							gateValues.put(g.getName(), gResult);
						}
						else {
							Boolean gResult = ((OnePinGate) g).process_input(id.getValueFor(g.getName()));
							gateValues.put(g.getName(), gResult);
						}
					} else if(g instanceof TwoPinGate){
						String pin1 = tokens[2];
						String pin2 = tokens[3];
						Boolean p1value = null, p2value = null;
						if(gateValues.containsKey(pin1)){
							p1value = gateValues.get(pin1);
						} else 	{
							p1value = id.getValueFor(g.getName() + "=" + pin1);
							gateValues.put(pin1, p1value);
						}
						if(gateValues.containsKey(pin2)){
							p2value = gateValues.get(pin2);
						} else 	{
							p2value = id.getValueFor(g.getName() + "=" + pin2);
							gateValues.put(pin2, p2value);
						}
						Boolean gResult = ((TwoPinGate) g).process_input(p1value, p2value);
						gateValues.put(g.getName(), gResult);
					}
				}
				//System.out.println(g.getName());
			}
		}catch (IOException e) {
			e.printStackTrace();
		}
	}
}
