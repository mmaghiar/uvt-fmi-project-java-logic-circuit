package circuit;

import gates.LogicalGate;

import java.util.HashMap;
import java.util.Map;
/**
 * class Circuit - used for keeping track of Logical Gates.
 * It is also used for retrieving a gate by its name.
 * @author adi
 *
 */
public class Circuit {
	Map<String, LogicalGate> gatesMap = new HashMap<String, LogicalGate>();
	
	
	public void addGate(LogicalGate g){
		gatesMap.put(g.getName(), g);
		
	}
	
	public LogicalGate getGateByName(String name){
		return gatesMap.get(name);
	}
}
