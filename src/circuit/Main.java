package circuit;

/**
 * Program:
 * Write a program simulating the flow of signals in some logical circuit composed by AND, OR, XOR and NOT gates. There can be many inputs and many outputs to collect.
 * 
 * @author Maghiar Mihai Adrian, Year 2, 2014.
 */

import input.csv.File;
import InputOutput.InputDevice;
import InputOutput.OutputDevice;


public class Main {
/**
 * The program will request you to input the starting values for the pin inputs
 * coming from outside the circuit.
 * @param args - no args are needed for execution of this program
 * @throws Exception
 */
	public static void main(String[] args){
		//TODO Please change the path to your local file destination in order the program to run
		java.io.File currDir = new java.io.File(".");
		    String path = currDir.getAbsolutePath();
		String circuitFile = path + "/logic_circuit.csv";
		File file = new File();
		InputDevice id = new InputDevice();
		OutputDevice od = new OutputDevice();
		file.run(circuitFile, id, od);
		
		
		
	}
}
