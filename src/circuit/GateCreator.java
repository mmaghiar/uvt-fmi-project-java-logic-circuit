package circuit;

import gates.ANDGate;
import gates.LogicalGate;
import gates.NOTGate;
import gates.ORGate;
import gates.XORGate;

public class GateCreator {
	
	public static LogicalGate create(String type, String name){

		if(type.equals("xor")) return new XORGate(name);
		if(type.equals("and")) return new ANDGate(name);
		if(type.equals("not")) return new NOTGate(name);
		if(type.equals("or")) return new ORGate(name);
		
		return null;
	}
}
