package gates;
/**
 * This class is used to define the gate's type and computing the inputs.
 * @author Maghiar Mihai Adrian
 *
 */
public class XORGate implements TwoPinGate{

	private String name = null;
	
	public XORGate(String name){
		this.name = name;
	}
	@Override
	public boolean process_input(boolean firstInput, boolean secondInput) {
		if(firstInput == secondInput)
			return true;
		return false;
	}
	
	public String getName(){
		return this.name;
	}
}
