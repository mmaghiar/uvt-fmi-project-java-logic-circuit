package gates;
/**
 * Interface used for defining the behavior of locagical gates with one input
 * @author Maghiar Mihai Adrian
 *
 */
public interface OnePinGate extends LogicalGate{
	public boolean process_input(boolean input);
	
}
