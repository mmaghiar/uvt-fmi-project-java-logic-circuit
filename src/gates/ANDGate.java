package gates;

/**
 * This class is used to define the gate's type and computing the inputs.
 * @author Maghiar Mihai Adrian
 *
 */
public class ANDGate implements TwoPinGate{
	private String name = null;
	/**
	 * 
	 * @param name - name of the gate taken as a identifier
	 */
	public ANDGate(String name){
		this.name = name;
	}
	@Override
	public boolean process_input(boolean firstInput, boolean secondInput) {
		return firstInput && secondInput;
	}
	public String getName(){
		return this.name;
	}



}
