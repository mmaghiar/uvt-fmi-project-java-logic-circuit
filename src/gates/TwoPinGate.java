package gates;
/**
 * Interface used for defining the behavior of locagical gates with two inputs.
 * @author Maghiar Mihai Adrian
 *
 */
public interface TwoPinGate extends LogicalGate{
	public boolean process_input(boolean firstInput, boolean secondInput);
}
