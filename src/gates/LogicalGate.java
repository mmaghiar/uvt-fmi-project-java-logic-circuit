package gates;
/**
 * Standard interface for logical gate
 * @author Maghiar Mihai Adrian
 *
 */
public interface LogicalGate {
	
	public String getName();
	
}
