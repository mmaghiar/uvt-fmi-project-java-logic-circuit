package gates;
/**
 * This class is used to define the gate's type and computing the inputs.
 * @author Maghiar Mihai Adrian
 *
 */
public class ORGate implements TwoPinGate{
	private String name = null;
	
	public ORGate(String name){
		this.name = name;
	}
	@Override
	public boolean process_input(boolean firstInput, boolean secondInput) {
		// TODO Auto-generated method stub
		return firstInput || secondInput;
	}
	public String getName(){
		return this.name;
	}

}
