package gates;
/**
 * This class is used to define the gate's type and computing the inputs.
 * @author Maghiar Mihai Adrian
 *
 */
public class NOTGate implements OnePinGate{
	private String name = null;
	
	public NOTGate(String name){
		this.name = name;
	}
	@Override
	public boolean process_input(boolean input) {
		// TODO Auto-generated method stub
		return !input;
	}
	public String getName(){
		return this.name;
	}

}
